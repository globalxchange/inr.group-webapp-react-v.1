import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import tradeIcon from '../assets/Trade.svg';
import { AppContext } from '../contexts/AppContext';
import Skeleton from 'react-loading-skeleton';
import LoadingAnimation from '../components/LoadingAnimation';
import GroupCarousel from '../components/GroupCarousel';
import ProgressiveImage from '../components/ProgressiveImage';
import PopupModalLayout from '../layout/PopupModalLayout';

const LandingPage = () => {
  const { appData } = useContext(AppContext);

  const [isReady, setIsReady] = useState(false);
  const [isIndexFundModalOpen, setIsIndexFundModalOpen] = useState(false);
  const [isAboutModalOpen, setIsAboutModalOpen] = useState(false);

  useEffect(() => {
    let timeout;
    if (appData) {
      timeout = setTimeout(() => {
        setIsReady(true);
      }, 500);
    }
    return () => {
      clearTimeout(timeout);
    };
  }, [appData]);

  return (
    <div className="landing-page-wrapper">
      <GroupCarousel className="d-sm" />
      {isReady || (
        <div className="loading-wrapper">
          <div className="loading-text">Loading Up Conglomerate</div>
          <LoadingAnimation />
        </div>
      )}
      <div className="landing-page">
        {appData ? (
          <ProgressiveImage
            src={appData?.logo}
            alt=""
            className="landing-full-logo"
          />
        ) : (
          <Skeleton width={900} height={200} />
        )}
        <img src={appData?.icon} alt="" className="landing-mobile-icon" />
        <img src={appData?.logo} alt="" className="landing-mobile-logo" />
        <div className="action-container">
          <div
            className="action-button"
            style={{
              borderColor: `#${appData?.other_data?.colorcode}`,
              color: `#${appData?.other_data?.colorcode}`,
            }}
            onClick={() => setIsAboutModalOpen(true)}
          >
            About
          </div>
          <Link
            to="/brands"
            className="action-button primary"
            style={{
              backgroundColor: `#${appData?.other_data?.colorcode}`,
              borderColor: `#${appData?.other_data?.colorcode}`,
            }}
          >
            Brands
          </Link>
          <div
            className="action-button investor-btn"
            style={{
              borderColor: `#${appData?.other_data?.colorcode}`,
              color: `#${appData?.other_data?.colorcode}`,
            }}
            onClick={() => setIsIndexFundModalOpen(true)}
          >
            Index
          </div>
        </div>
      </div>
      <div className="index-funds-button">
        <img
          src={require('../assets/invester-fund-icon.svg').default}
          alt=""
          className="funds-icon"
        />
      </div>
      <GroupCarousel className="d-lg" />
      <PopupModalLayout
        isOpen={isIndexFundModalOpen}
        onClose={() => setIsIndexFundModalOpen(false)}
        noHeader
        width={500}
      >
        <div className="no-web-popup">
          <ProgressiveImage
            src={require('../assets/invester-fund-icon.svg').default}
            alt=""
            className="app-logo"
          />
          <div
            className="bottom-bar"
            style={{
              backgroundColor: `#${appData?.other_data?.colorcode}`,
              fontSize: 18,
            }}
          >
            Coming Soon
          </div>
        </div>
      </PopupModalLayout>
      <PopupModalLayout
        isOpen={isAboutModalOpen}
        onClose={() => setIsAboutModalOpen(false)}
        noHeader
        width={500}
      >
        <div className="app-details-modal">
          <ProgressiveImage
            src={appData?.logo}
            alt=""
            className="landing-full-logo"
          />
          <div className="app-desc">{appData?.description}</div>
          <Link
            className="no-of-brands"
            style={{
              backgroundColor: `#${appData?.other_data?.colorcode}`,
            }}
            to="/brands"
          >
            {appData?.brands?.length || 0} Brands
          </Link>
        </div>
      </PopupModalLayout>
    </div>
  );
};

export default LandingPage;
