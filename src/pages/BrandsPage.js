import { useContext, useEffect, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import { Link } from 'react-router-dom';
import ProgressiveImage from '../components/ProgressiveImage';
import { AppContext } from '../contexts/AppContext';

const BrandsPage = () => {
  const { appData, brandList } = useContext(AppContext);

  const [searchInput, setSearchInput] = useState('');
  const [filteredList, setFilteredList] = useState([]);

  useEffect(() => {
    if (brandList) {
      const searchQuery = searchInput.trim().toLowerCase();

      const list = brandList?.filter(
        (x) =>
          x.brand_code.toLowerCase().includes(searchQuery) ||
          x.description.toLowerCase().includes(searchQuery) ||
          x.name.toLowerCase().includes(searchQuery),
      );

      setFilteredList(list);
    }
  }, [brandList, searchInput]);

  return (
    <div className="brands-page-wrapper">
      <div className="header-container">
        <Link to="/" className="heder-link">
          {appData ? (
            <ProgressiveImage
              src={appData?.logo}
              alt=""
              className="header-logo"
            />
          ) : (
            <Skeleton width={700} height={200} />
          )}
        </Link>
        <div className="search-container">
          <input
            type="text"
            className="search-input"
            placeholder="Search Our Brands..."
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
          />
        </div>
      </div>
      <div className="brands-list-container">
        {filteredList?.map((item) => (
          <Link
            to={`/brands/${item.brand_code}`}
            key={item.brand_code}
            className="brand-item-container"
          >
            <div className="brand-item">
              {item?.other_data?.coloredfulllogo ? (
                <ProgressiveImage
                  src={item?.other_data?.coloredfulllogo}
                  className="brand-logo"
                  height={80}
                  width={250}
                />
              ) : (
                <div className="brand-coming-soon">Coming Soon</div>
              )}
              {item.name && (
                <div
                  className="brand-name"
                  style={{
                    backgroundColor: `#${item.other_data?.primarycolourcode}`,
                  }}
                >
                  {item.name}
                </div>
              )}
            </div>
          </Link>
        ))}
      </div>
    </div>
  );
};

export default BrandsPage;
