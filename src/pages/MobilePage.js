import React, { useContext, useMemo, useState } from 'react';
import { Link } from 'react-router-dom';
import ProgressiveImage from '../components/ProgressiveImage';
import { AppContext } from '../contexts/AppContext';
import PopupModalLayout from '../layout/PopupModalLayout';

const MobilePage = ({ selectedApp }) => {
  const { appData, brandList } = useContext(AppContext);

  const [isNoWebistePopupOpen, setIsNoWebistePopupOpen] = useState(false);

  const list = useMemo(() => {
    if (!brandList) {
      return [];
    }

    const groupItem = { ...appData, appPath: '/' };
    // Centering Item
    const currentIndex = brandList.findIndex(
      (x) => x.name === selectedApp.name,
    );

    if (currentIndex < 0 || currentIndex === 0) {
      return [groupItem, ...brandList];
    }
    const parsedList = [...brandList];
    const tempItem = parsedList[0];
    parsedList[0] = parsedList[currentIndex];
    parsedList[currentIndex] = tempItem;

    return [groupItem, ...parsedList];
  }, [brandList, selectedApp, appData]);

  const colorCode = useMemo(() => {
    let color = '';

    if (selectedApp?.other_data?.primarycolourcode) {
      color = `#${selectedApp?.other_data?.primarycolourcode}`;
    } else if (selectedApp?.other_data?.secondarycolourcode) {
      color = `#${selectedApp?.other_data?.secondarycolourcode}`;
    }

    return color;
  }, [selectedApp]);

  const onLinkClick = () => {
    if (!selectedApp?.website) {
      const win = window.open(selectedApp?.website, '_blank');
      if (win != null) {
        win.focus();
      }
    } else {
      setIsNoWebistePopupOpen(true);
    }
  };

  return (
    <div className="mobile-page-wrapper">
      <div className="header-container">
        <ProgressiveImage
          src={selectedApp?.other_data?.coloredfulllogo}
          alt=""
          className="header-img"
          height={70}
          width={300}
        />
        <div className="hello-text">Hello, I Am {selectedApp?.name}</div>
        <div className="brand-desc">{selectedApp.description}</div>
      </div>
      <div className="main-content-container">
        <div className="video-container">
          <div className="video-thumbnail">
            <svg
              className="play-icon"
              width="78"
              height="87"
              viewBox="0 0 78 87"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M75.79 39.7069L6.45667 0.556966C5.11333 -0.199933 3.47533 -0.182533 2.14933 0.591766C0.814667 1.37476 0 2.80156 0 4.35016V82.65C0 84.1986 0.814667 85.6254 2.14933 86.4084C2.82533 86.7999 3.57933 87 4.33333 87C5.06133 87 5.798 86.8173 6.45667 86.4432L75.79 47.2933C77.1507 46.519 78 45.0748 78 43.5001C78 41.9254 77.1507 40.4812 75.79 39.7069Z"
                fill={colorCode || '#08152D'}
              />
            </svg>
          </div>
          <div className="video-meta">
            <div className="video-title">Introducing {selectedApp?.name}</div>
            <div className="video-author">
              By :{' '}
              <div className="author-name">
                <img src={appData?.icon} alt="" className="author-icon" />{' '}
                {appData?.groupname}
              </div>
            </div>
            <div className="video-desc">
              {selectedApp.description.substring(0, 100)}
            </div>
          </div>
        </div>
        <div className="redirect-btn" onClick={onLinkClick}>
          Done Watching? <span>Click Here</span>
        </div>
      </div>
      <div className="controller-container">
        {list.map((app, i) => (
          <Link
            key={app.name}
            className={`app-item ${
              selectedApp?.name === app.name ? 'active' : ''
            } `}
            to={app.appPath || `/brands/${app.brand_code}`}
          >
            <img
              src={app?.icon || app?.colored_icon}
              alt=""
              className="app-icon"
            />
          </Link>
        ))}
      </div>
      <PopupModalLayout
        isOpen={isNoWebistePopupOpen}
        onClose={() => setIsNoWebistePopupOpen(false)}
        noHeader
        width={700}
      >
        <div className="no-web-popup">
          <ProgressiveImage
            src={selectedApp?.other_data?.coloredfulllogo}
            alt=""
            className="app-logo"
            width={35}
            height={35}
          />
          <div className="bottom-bar" style={{ backgroundColor: colorCode }}>
            The Website For {selectedApp?.name} Will Be Up Soon
          </div>
        </div>
      </PopupModalLayout>
    </div>
  );
};

export default MobilePage;
