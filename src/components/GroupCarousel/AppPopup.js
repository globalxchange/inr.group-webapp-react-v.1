import PopupModalLayout from '../../layout/PopupModalLayout';

const AppPopup = ({ isOpen, onClose, appData }) => {
  const openWebsite = () => {
    onClose();
    if (appData.website === '#') {
      return;
    }
    const win = window.open(appData.website, '_blank');
    if (win != null) {
      win.focus();
    }
  };

  return (
    <PopupModalLayout isOpen={isOpen} onClose={onClose} noHeader>
      <div className="app-popup-container">
        <img className="app-log" alt="" src={appData?.logo} />
        <div className="app-desc">{appData?.description}</div>
        <div
          className="website-btn"
          onClick={openWebsite}
          style={{ backgroundColor: `#${appData?.other_data?.colorcode}` }}
        >
          Website
        </div>
      </div>
    </PopupModalLayout>
  );
};

export default AppPopup;
