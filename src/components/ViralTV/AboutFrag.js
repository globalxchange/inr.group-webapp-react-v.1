const AboutFrag = ({ selectedApp, colorCode }) => (
  <div className="about-frag">
    <div className="header-container">
      <img src={selectedApp?.colored_icon} alt="" className="app-icon" />
      <div className="app-details">
        <div className="app-name">What Is {selectedApp?.name}?</div>
        <div className="app-owner">By {selectedApp?.email}</div>
      </div>
    </div>
    <div className="app-desc">{selectedApp?.description}</div>
    <div className="action-container">
      <div className="action-item" style={{ backgroundColor: colorCode }}>
        Get Started
      </div>
    </div>
  </div>
);

export default AboutFrag;
