import React, { useState } from 'react';

const Player = ({ colorCode }) => {
  const [isStartPlay, setIsStartPlay] = useState(false);

  return (
    <div className="player-container">
      <div className="player-cover">
        <svg
          className="play-icon"
          width="78"
          height="87"
          viewBox="0 0 78 87"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M75.79 39.7069L6.45667 0.556966C5.11333 -0.199933 3.47533 -0.182533 2.14933 0.591766C0.814667 1.37476 0 2.80156 0 4.35016V82.65C0 84.1986 0.814667 85.6254 2.14933 86.4084C2.82533 86.7999 3.57933 87 4.33333 87C5.06133 87 5.798 86.8173 6.45667 86.4432L75.79 47.2933C77.1507 46.519 78 45.0748 78 43.5001C78 41.9254 77.1507 40.4812 75.79 39.7069Z"
            fill={colorCode || '#08152D'}
          />
        </svg>
      </div>
    </div>
  );
};

export default Player;
