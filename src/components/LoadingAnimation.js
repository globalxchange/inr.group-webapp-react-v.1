const LoadingAnimation = () => (
  <div className="loading-wrapper">
    <img
      src={require('../assets/venture-markets-logo.svg').default}
      alt=""
      className="loading-img"
    />
    <img
      src={require('../assets/powered-by-image.svg').default}
      alt=""
      className="powered-by"
    />
  </div>
);

export default LoadingAnimation;
